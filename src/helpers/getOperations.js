'use strict';

const glob = require('globby');
const path = require('path');

module.exports = function getOperations (workingDirectory, roots, ignoreSymlinks) {
	return glob(
		Array.isArray(roots) ?
			roots.map(root => path.join(root, '**', 'operations*.json')) :
			[
				'packages/**/operations*.json',
				'packages-shared/**/operations*.json',
				'platform-linked/**/operations*.json',
				'platform/**/operations*.json'
			],
		{
			cwd: workingDirectory,
			follow: !ignoreSymlinks
		})
		.then(operationsJsons => {
			return operationsJsons.reduce((allOps, operationsJson) => {
				const operations = require(path.join(workingDirectory, operationsJson));
				return allOps.concat(Object.keys(operations)
					.filter(operationName => !!operations[operationName])
					.map(operationName => {
						const operation = operations[operationName];
						operation.operationName = operationName;
						operation.operationFile = operationsJson;
						return operation;
					})
				);
			}, []);
		});
};
