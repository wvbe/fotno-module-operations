'use strict';

const getOperations = require('./getOperations');

function getOperationRegistry (workingDirectory, roots) {
	return getOperations(workingDirectory, roots)
		.then(operationList => operationList.reduce((mapping, operation) => {
			if (!mapping[operation.operationName]) {
				mapping[operation.operationName] = operation;
			}

			return mapping;
		}, {}));
}

function expandOperationDefinition (mapping, operationName) {
	if (!mapping[operationName]) {
		throw new Error(`Operation "${operationName}" is not registered.`);
	}

	const operation = Object.assign({}, mapping[operationName]);

	['steps', 'getStateSteps'].forEach(prop => {
		if (!operation[prop]) {
			return;
		}

		if (!Array.isArray(operation[prop])) {
			operation[prop] = [operation[prop]];
		}

		operation[prop] = operation[prop].map(step => {
			const type = step.type.split('/');
			if (type[0] === 'operation' && type[1]) {
				step.definition = expandOperationDefinition(mapping, type[1]);
			}

			return step;
		});
	});

	return operation;
}

module.exports = function getOperation (workingDirectory, operationName, roots, _ignoreSymlinks) {
	return getOperationRegistry(workingDirectory, roots)
		.then(mapping => expandOperationDefinition(mapping, operationName));
};
