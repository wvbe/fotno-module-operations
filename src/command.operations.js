'use strict';

const Table = require('@fontoxml/fontoxml-development-tools-module-core').TableCommand;

const getOperations = require('./helpers/getOperations');

module.exports = (fotno) => {
	const config = fotno.registerConfiguration(
		'operations',
		{
			defaultColumns: [
				'name',
				'file',
				'label'
			]
		});

	const columns = [
		{
			name: 'name',
			label: 'Name',
			value: (operation) => operation.operationName
		},
		{
			name: 'file',
			label: 'File',
			value: (operation) => operation.operationFile
		},
		{
			name: 'label',
			label: 'Label',
			value: (operation) => operation.label
		},
		{
			name: 'desc',
			label: 'Description',
			value: (operation) => operation.description
		},
		{
			name: 'key',
			label: 'Key binding',
			value: (operation) => operation.keyBinding
		}
	];

	const table = new Table(fotno, columns.map(col => Object.assign(col, {
			default: config.defaultColumns.indexOf(col.name) >= 0
		})));

	function operationsCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		let destroySpinner = res.spinner('Looking up operations...');

		return getOperations(req.fdt.editorRepository.path, req.options.roots.length ? req.options.roots : null)
			.then(operations => {
				destroySpinner();
				destroySpinner = null;
				res.break();
				table.print(
					res,
					req.options.columns,
					operations,
					req.options.sort,
					req.options.export);
			})
			.catch(() => {
				if (destroySpinner) {
					destroySpinner();
				}
			});
	}

	fotno.registerCommand('operations', operationsCommand)
		.addAlias('list-operations')
		.setDescription('Output a table with information about all operations in a repository.')

		.addOption(new fotno.MultiOption('roots')
			.setShort('r')
			.setDescription('Root folder(s).')
		)
		.addOption(table.exportOption)
		.addOption(table.columnsOption)
		.addOption(table.sortOption)

		.addExample(`${fotno.getAppInfo().name} operations`, 'Output all operations configured in the application.');
};
