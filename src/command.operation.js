'use strict';

const getOperation = require('./helpers/getOperation');

function recurseSteps (res, operation, isGetStateSteps) {
	const prop = isGetStateSteps && operation.getStateSteps ?
		'getStateSteps' :
		'steps';

	if (!operation[prop]) {
		return;
	}

	operation[prop].forEach(step => {
		const type = step.type.split('/');

		res.property(type[0], type[1] || '-', 20);

		if (!step.definition) {
			return;
		}

		res.indent();
		recurseSteps(res, step.definition, isGetStateSteps);
		res.outdent();
	});
}

function echoOperationSummary (_req, res, operationSummary) {
	res.properties({
		Name: operationSummary.operationName,
		File: operationSummary.operationFile,
		Label: operationSummary.label || '-',
		Description: operationSummary.description || '-',
		Keys: (operationSummary.keyBinding || '-') + (operationSummary.keyBindingDisabled ? ' (disabled)' : ''),
		Icon: operationSummary.icon || '-',
		Critical: operationSummary.errorsAreFatal ? 'Yes' : 'No'
	});

	if (operationSummary.initialData) {
		res.caption('Initial data');
		res.indent();
		res.debug(operationSummary.initialData);
		res.outdent();
	}

	if (operationSummary.getStateSteps) {
		res.caption('State steps');
		res.indent();
		recurseSteps(res, operationSummary, true);
		res.outdent();
	}

	if (operationSummary.steps) {
		res.caption('Steps');
		res.indent();
		recurseSteps(res, operationSummary, false);
		res.outdent();
	}

	if (operationSummary.alternatives) {
		res.caption('Alternatives');
		res.list(operationSummary.alternatives);
	}
}

module.exports = fotno => {
	function operationCommand (req, res) {
		res.caption(req.command.getLongName());

		req.fdt.editorRepository.throwIfNotInsideEditorRepository();

		let destroySpinner = res.spinner('Looking up operation...');

		return getOperation(
			req.fdt.editorRepository.path,
			req.parameters.operation,
			null,
			req.options['no-follow'])
			.then(operation => {
				destroySpinner();
				destroySpinner = null;
				res.break();
				echoOperationSummary(req, res, operation);
			})
			.catch(() => {
				if (destroySpinner) {
					destroySpinner();
				}
			});
	}

	fotno.registerCommand('operation', operationCommand)
		.setDescription('Output information about a specific operation.')

		.addParameter('operation', 'The operation name.', true)

		.addExample(`${fotno.getAppInfo().name} operation delete-node`, 'Output information about the "delete-node" operation in the application.');
};
