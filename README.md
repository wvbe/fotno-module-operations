# fontoxml-development-tools-module-operations

The Development tools for FontoXML can support a developer when configuring a FontoXML editor.
This module contains commands for showing information about configured operations.

## Installation

This module is part of the FontoXML development tools and is not meant to be used separately. To install the FontoXML development tools, run the following command:

	npm i -g @fontoxml/fontoxml-development-tools

### Usage examples

	fdt operations [--columns name file label desc key ...]

Output a table with information about all operations in a repository.

	fdt operation <operationName>

Output information about a specific operation.
