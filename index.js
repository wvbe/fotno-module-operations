module.exports = fotno => {
	[
		require('./src/command.operations'),
		require('./src/command.operation')
	].forEach(mod => mod(fotno));
};
